# Conditions Générales d'Utilisation (2016.02.08) - Mise à jour effectuée le 25 Juin 2017

Il se doit d'être clair que, pour tout utilisateurs

- Ceci n'est qu'une **archive et non un point de référence**
- Cette archive ne prétends connaître la vérité
- Cette archive se dégage de responsabilitées à l'égard de son utilisation ainsi que des résultats de la consommation du matériel diffusé

_Si quelque chose vous déplaît_, **il est dans votre ~~devoir~~ _obligation_ de le faire savoir** rapidement en communiquant avec l'[Administrateur](mailto:mathieu@zeroserieux.com).

### Formats

Puisque certains formats audio et vidéo ne sont adaptés à la diffusion _en-ligne_, il est possible que la lecture de certains fichiers soit difficile à partir de l'archive.
Si vous avez des problèmes de visionnement _en-ligne_, sauvegardez tout simplement les fichiers directement sur votre ordinateur plutôt que de les visionner à la volée.
Ils seronts, par la même occasion, disponible pour consommation ultérieure directement de votre ordinateur en cas de catastrophe majeure telle:

- Une rage de visionnement du contenu de l'Archive
- Un cyclone innatendu au large des côtes septentrionnales 

_Suggestion_: Utilisez _*VLC*_ pour la lecture des fichiers video/audio

_*VLC*_ est disponible via le [site _officiel_ de _VideoLAN_](https://www.videolan.org/vlc)

### Comment faire enlever du matériel de cette archive?

Écrivez à l'[Administrateur](mailto:mathieu@zeroserieux.com) avec les informations suivantes:

- Votre status
- Vos motifs
- Le ou les liens concernés

Suite à l'analyse de vos motifs et l'approbation du conseil général de moi-même, les fichiers seront retirés de la liste de diffusion _en-ligne_

### Comment suggérer du contenu

Écrivez à l'[Administrateur](mailto:mathieu@zeroserieux.com) indiquant clairement

- Votre status
- La/les locations (endroits) ou se trouve ledit matériel

Sous toutes réserves et après approbation du conseil de moi-même (dans toute mon intégralitée), il sera ajouté dans un délai de moins de 24 heures.

### Autres

Questions/commentaires/demandes d'informations/peanuts/chocolat/popcorn? Veuillez communiquer avec l'[Administrateur](mailto:mathieu@zeroserieux.com).

Merci et... partagez!
